$(function () {
    $('.btn-container input').on('click', function () {
        if (this.value !== "=" && this.value !== 'CE') {
            $('#screen').val($('#screen').val() + this.value);
        } else if(this.value === '=') {
            data = {num: $('#screen').val()};

            fetch('http://127.0.0.1/calculator/serverCalculator.php',
                {
                    method: 'post',
                    body: data
                }
            ).then((respond)=>{
                $('#screen').val(respond);
            })
        }else {
            $('#screen').val('');
        }
    })
})